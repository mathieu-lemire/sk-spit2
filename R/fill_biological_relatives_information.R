
# need to use na="" when write.table 

fill_biological_relatives_information <-
  function(datafile, output=sub(".csv$","_biorel_filled.csv", datafile) , complete_code_non_valid_sib_1reg=2 ){
    
    
    chk<- spit2CheckVersion()
    if( chk==0 ){
      return(NULL)
    }
    
    
    # logging, no need to split to screen as well 
    logfile<- sub(".csv$",".log.txt", datafile)  
    sink( file=logfile, append=TRUE , split=F )
    cat("---------------------------------------------------------------------------------\n")
    cat("sessionInfo\n")
    cat("---------------------------------------------------------------------------------\n\n")
    print( sessionInfo() ) 
    cat("\n\n---------------------------------------------------------------------------------\n")
    cat("match.call\n")
    cat("---------------------------------------------------------------------------------\n\n")
    print( match.call() )
    argg <- c(as.list(environment()) )
    print(argg)
    cat("\n\n---------------------------------------------------------------------------------\n")
    cat("output\n---------------------------------------------------------------------------------\n\n")
    
    sink() 
    sink( file=logfile, append=TRUE, split=T )
    
    
    # this is the info written down in biorel_rscript_tracking to keep track of the original file name 
    datafile.nopath<- tail( unlist( strsplit( datafile, "/")  ), 1 )
    spit2.version<- packageVersion("spit2")
    # the pc_rscript will be filled with this value, which lists the file name that contains the 
    # original data and the version of spit2 
    biorelscript.value<- paste( datafile.nopath, spit2.version, sep=";") 
    
    
    dat<- read.csv(datafile, na.strings="" ,as.is=TRUE ) 
    
    
    
    # checking which dataset it is by comparing column names to the above 
    tmp<- which_biorel_data( names(dat) )
    if( is.null(tmp) ){ return(NULL) }
    
    which.biorel<- tmp[["which.biorel"]]
    information_complete_column <- tmp[["information_complete_column"]]
    # spaghetti code
    is.summer2019<- tmp[["summer2019"]] > tmp[["fall2019"]]
    
    if( is.summer2019 ){ dat$biorel_rscript_tracking<-NA  }    
    
    dat<- dat[ is.na( dat$biorel_rscript_tracking) , ]
    dat$biorel_rscript_tracking <- biorelscript.value 
    
    
    # select samples with sib_sameinfo 
    # ignoring samples for which biorel values are not all 0 
    
    
    
    sel.sib_sameinfo<- dat$sib_sameinfo == 1  & !is.na( dat$sib_sameinfo )  & 
      apply(  is.na( dat[,which.biorel]) | dat[,which.biorel]==999 | dat[,which.biorel]==0, 1, all ) 
    
    nsibs<- sum( sel.sib_sameinfo )
    cat( "Need to fetch biorelative information for", nsibs, ifelse( nsibs==1, "sib" , "sibs") )
    
    #if( sum(sel.sib_sameinfo)==0 ){
    #  cat(".\n**Nothing to be done, no output generated. Exiting.\n")
    #  return( NULL )
    #}
    
    dat.out<- c() 
    if(  sum(sel.sib_sameinfo) > 0 ){
      
      dat.sib_sameinfo<- dat[ sel.sib_sameinfo ,] 
      nfam<- length( unique( dat.sib_sameinfo$family_id ) )
      cat( " from", nfam, ifelse( nfam==1 , "family", "families.\n") )
      
      sib_1reg_warning<-FALSE 
      sib_1reg_warning2<-FALSE 
      
      for( i in 1:nrow( dat.sib_sameinfo ) ){
        famid<-as.character( dat.sib_sameinfo$family_id[ i ]  )
        sel.sib_1reg<- which(  dat$family_id==famid & dat$sib_1reg== 1 )
        # cat( length( sel.sib_1reg ) ," ",i,"\n")
        if( length( sel.sib_1reg )==0 ){
          sib_1reg_warning<-TRUE 
          cat("Check family", famid," for participant", dat.sib_sameinfo$participant_id[ i ],  
              ". I could not find a valid sib_1reg.\n")
          dat.sib_sameinfo[i, information_complete_column  ] <- complete_code_non_valid_sib_1reg
          # removing the value of biorel_rscript_tracking
          dat.sib_sameinfo$biorel_rscript_tracking[i]<- NA
          dat$biorel_rscript_tracking[ dat$family_id==famid ]<- NA
          
        } else if( length( sel.sib_1reg ) > 1 ){
          # select sibs
          sib_1reg_warning2=TRUE 
          cat("Check family", famid,": participants " , 
              paste( dat$participant_id[ sel.sib_1reg] , collapse="," ),
              " all have sib_1ref=1.\n" )
          dat.sib_sameinfo$biorel_rscript_tracking[i]<- NA
          dat$biorel_rscript_tracking[ dat$family_id==famid ]<- NA
        } else {
          dat.sib_sameinfo[i, which.biorel ]<- dat[ sel.sib_1reg , which.biorel ]
          dat.sib_sameinfo[i, information_complete_column ] <- 2 
          dat[ sel.sib_1reg , information_complete_column ] <- 2 
        }
      }
      
      if( sib_1reg_warning ){
        cat( "\n** The above participant without valid sib_1reg companion will be found in the output\n")
        cat( paste0( "** with ", information_complete_column, " set to ", complete_code_non_valid_sib_1reg, ".\n") )
        cat( "** and with biorel_rscript_tracking set to blank.\n" ) 
        cat( "** This can be changed manually.\n") 
      }
      if( sib_1reg_warning2 ){
        cat( "\n** The above families with multiple sib_1reg=1 will NOT be found in the output.")
        cat( "\n** Please correct manually if necessary.\n")
      }
      dat.out<-  dat.sib_sameinfo[, c( "participant_id", which.biorel,"biorel_rscript_tracking",information_complete_column) ]
      
      
    } 
    
    
    
    dat.others<- dat[ !is.element( dat$participant_id , dat.out$participant_id),  
                      c( "participant_id", which.biorel,"biorel_rscript_tracking",information_complete_column ) ]
    
    dat.out<- rbind( dat.out, dat.others )
    
    # adding a column scores_auto_calculated_complete
    # as of 200210 this is populated from ancestry completeness alone .it takes 0 if there are missing data
    
    gp.complete<- apply( data.frame( c( "mgm","mgf", "pgm","pgf") ), 1, 
                         function( gp ){
                           as.numeric(apply( dat.out[, grep( gp, names(dat.out) )] > 0  , 1 , any ) )
                         }
    )
    
    autocalc <- 2* as.numeric(apply( gp.complete > 0 , 1, all ) )
    dat.out$scores_auto_calculated_complete <- autocalc 
    dat.out[ dat.out$scores_auto_calculated_complete==0, information_complete_column ] <- 0
    
    cat("\nWriting to file", output,"\n") 
    
    if( is.summer2019 ){
      dat.out <- dat.out[ , !is.element( names(dat.out), "biorel_rscript_tracking" ) ]
    }
    write.table(dat.out , output , sep=",", quote=TRUE , col=TRUE , row=FALSE, na="" )
    
    sink()

    invisible( dat.out ) 
    
  }



which_biorel_data<-function( namesdat ){
  # names associated with the biorelative instrument
  which.biorel.summer2019 <-
    c("mgm_999___e1a1", "mgm_white___e1a20", "mgm_jewish___1", "mgm_jewish_s___e1a18", 
      "mgm_jewish_s___e1a19", "mgm_black___1", "mgm_black_s___e1a4", 
      "mgm_black_s___e1a2", "mgm_black_s___e1a3", "mgm_black_s___e1a5", 
      "mgm_latin___1", "mgm_latin_s___e1a5", "mgm_latin_s___e1a6", 
      "mgm_latin_s___e1a7", "mgm_asian___1", "mgm_asian_s___e1a10", 
      "mgm_asian_s___e1a11", "mgm_asian_s___e1a12", "mgm_asian_s___e1a13", 
      "mgm_southasian___e1a14", "mgm_southeastasian___e1a15", "mgm_indig___e1a8", 
      "mgm_pacific___e1a9", "mgm_westasian___e1a16", "mgm_arab___e1a17", 
      "mgf_999___e2a1", "mgf_white___e2a20", "mgf_jewish___1", "mgf_jewish_s___e2a18", 
      "mgf_jewish_s___e2a19", "mgf_black___1", "mgf_black_s___e2a4", 
      "mgf_black_s___e2a2", "mgf_black_s___e2a3", "mgf_black_s___e2a5", 
      "mgf_latin___1", "mgf_latin_s___e2a5", "mgf_latin_s___e2a6", 
      "mgf_latin_s___e2a7", "mgf_asian___1", "mgf_asian_s___e2a10", 
      "mgf_asian_s___e2a11", "mgf_asian_s___e2a12", "mgf_asian_s___e2a13", 
      "mgf_southasian___e2a14", "mgf_southeastasian___e2a15", "mgf_indig___e2a8", 
      "mgf_pacific___e2a9", "mgf_westasian___e2a16", "mgf_arab___e2a17", 
      "pgm_999___e3a1", "pgm_white___e3a20", "pgm_jewish___1", "pgm_jewish_s___e3a18", 
      "pgm_jewish_s___e3a19", "pgm_black___1", "pgm_black_s___e3a4", 
      "pgm_black_s___e3a2", "pgm_black_s___e3a3", "pgm_black_s___e3a5", 
      "pgm_latin___1", "pgm_latin_s___e3a5", "pgm_latin_s___e3a6", 
      "pgm_latin_s___e3a7", "pgm_asian___1", "pgm_asian_s___e3a10", 
      "pgm_asian_s___e3a11", "pgm_asian_s___e3a12", "pgm_asian_s___e3a13", 
      "pgm_southasian___e3a14", "pgm_southeastasian___e3a15", "pgm_indig___e3a8", 
      "pgm_pacific___e3a9", "pgm_westasian___e3a16", "pgm_arab___e3a17", 
      "pgf_999___e4a1", "pgf_white___e4a20", "pgf_jewish___1", "pgf_jewish_s___e4a18", 
      "pgf_jewish_s___e4a19", "pgf_black___1", "pgf_black_s___e4a4", 
      "pgf_black_s___e4a2", "pgf_black_s___e4a3", "pgf_black_s___e4a5", 
      "pgf_latin___1", "pgf_latin_s___e4a5", "pgf_latin_s___e4a6", 
      "pgf_latin_s___e4a7", "pgf_asian___1", "pgf_asian_s___e4a10", 
      "pgf_asian_s___e4a11", "pgf_asian_s___e4a12", "pgf_asian_s___e4a13", 
      "pgf_southasian___e4a14", "pgf_southeastasian___e4a15", "pgf_indig___e4a8", 
      "pgf_pacific___e4a9", "pgf_westasian___e4a16", "pgf_arab___e4a17", 
      "famhx_screen", "famhx2_adhd___1", "famhx2_adhd___2", "famhx2_asd___1", 
      "famhx2_asd___2", "famhx2_anx___1", "famhx2_anx___2", "famhx2_bipolar___1", 
      "famhx2_bipolar___2", "famhx2_dep___1", "famhx2_dep___2", "famhx2_id___1", 
      "famhx2_id___2", "famhx2_ld___1", "famhx2_ld___2", "famhx2_ocd___1", 
      "famhx2_ocd___2", "famhx2_schizo___1", "famhx2_schizo___2", "famhx2_subab___1", 
      "famhx2_subab___2", "famhx2_ticstou___1", "famhx2_ticstou___2", 
      "famhx2_other___1", "famhx2_other___2" )
  
  
  # column names were changed fall 2019
  # note that biorel_rscript_tracking won't be found pre-fall2019
  which.biorel.fall2019 <- c("mgm___999_e1a1", "mgm___white_e1a20", "mgm___jewish_1", "mgm___black_1", 
                             "mgm___latin_1", "mgm___asian_1", "mgm___southasian_e1a14", "mgm___southeastasian_e1a15", 
                             "mgm___indig_e1a8", "mgm___pacific_e1a9", "mgm___westasian_e1a16", 
                             "mgm___arab_e1a17", "mgf___999_e2a1", "mgf___white_e2a20", "mgf___jewish_1", 
                             "mgf___black_1", "mgf___latin_1", "mgf___asian_1", "mgf___southasian_e2a14", 
                             "mgf___southeastasian_e2a15", "mgf___indig_e2a8", "mgf___pacific_e2a9", 
                             "mgf___westasian_e2a16", "mgf___arab_e2a17", "pgm___999_e3a1", 
                             "pgm___white_e3a20", "pgm___jewish_1", "pgm___black_1", "pgm___latin_1", 
                             "pgm___asian_1", "pgm___southasian_e3a14", "pgm___southeastasian_e3a15", 
                             "pgm___indig_e3a8", "pgm___pacific_e3a9", "pgm___westasian_e3a16", 
                             "pgm___arab_e3a17", "pgf___999_e4a1", "pgf___white_e4a20", "pgf___jewish_1", 
                             "pgf___black_1", "pgf___latin_1", "pgf___asian_1", "pgf___southasian_e4a14", 
                             "pgf___southeastasian_e4a15", "pgf___indig_e4a8", "pgf___pacific_e4a9", 
                             "pgf___westasian_e4a16", "pgf___arab_e4a17", "famhx_screen", 
                             "famhx2_adhd___1", "famhx2_adhd___2", "famhx2_asd___1", "famhx2_asd___2", 
                             "famhx2_bipolar___1", "famhx2_bipolar___2", "famhx2_id___1", 
                             "famhx2_id___2", "famhx2_ocd___1", "famhx2_ocd___2", "famhx2_schizo___1", 
                             "famhx2_schizo___2", "famhx2_other___1", "famhx2_other___2" )
  
  
  
  # which data set are we using 
  summer2019<- sum( is.element( namesdat, which.biorel.summer2019 ) )
  fall2019<- sum( is.element( namesdat, which.biorel.fall2019  ) )
  
  # checking that the data contains the necessary fields -- only for fall2019 
  if( fall2019 > summer2019 ){
    if( any( !is.element( c("mgm___999_e1a1","biorel_rscript_tracking" ,"scores_auto_calculated_complete","family_id"), namesdat ) ) ){
      cat("Some columns are missing from file\n  ", datafile,"\n" )
      cat("Check that the following instruments were all exported to the file:\n")
      cat("  Participant Information\n  Biological Relatives Information\n  Scores Auto Calculated\n")
      cat("\n\nExiting\n")
      return( NULL )
    }
  } else {
    if( any( !is.element( c("mgm_999___e1a1","scores_auto_calculated_complete","family_id"), namesdat ) ) ){
      cat("Some columns are missing from file\n  ", datafile,"\n" )
      cat("Check that the following instruments were all exported to the file:\n")
      cat("  Participant Information\n  Biological Relatives Information\n  Scores Auto Calculated\n")
      cat("\n\nExiting\n")
      return( NULL )
    }
  }
  which.biorel<- namesdat[ is.element( namesdat, c( which.biorel.fall2019, which.biorel.summer2019 ) ) ]
  
  information_complete_column<- c("onsite_biological_relatives_information_complete", "biological_relatives_information_complete" )
  # which one to keep?
  information_complete_column<- information_complete_column[ is.element( information_complete_column, namesdat )] 
  
  li<- list( which.biorel=which.biorel,
             information_complete_column=information_complete_column,
             summer2019=summer2019,
             fall2019=fall2019)
  
  return( li )
  
  
  
}




